#!/bin/bash

# Ensure maim, xclip and their dependencies are installed

cmd=$1

case $cmd in
  screenshot)
    exec maim $HOME/Pictures/$(date +%s)-screenshot.png
    ;;
  selection)
    exec maim -s $HOME/Pictures/$(date +%s)-selection.png
    ;;
  screencopy)
    exec maim | xclip -selection clipboard -t image/png
    ;;
  selectcopy)
    exec maim -s | xclip -selection clipboard -t image/png
    ;;
  pixelgrab)
    exec maim -st 0 | convert - -resize 1x1\! -format '%[pixel:p{0,0}]' info:- | xclip -selection clipboard
    ;;
esac
