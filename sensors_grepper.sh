#!/bin/bash

GREPPER_BLOB=$1
ACTION=$2

grep_output=$(sensors | grep -iP "$GREPPER_BLOB" | awk '{print $2}'  |  grep -Po '\d+\.\d+')
case $ACTION in 
  sum)
    val=0
    for i in $grep_output; do
      val=$(echo "scale=1; $val + $i" | bc -l)
    done
    echo $val
    ;;
  avg)
    val=0
    cnt=0
    for i in $grep_output; do
      val=$(echo "scale=1; $val + $i" | bc -l)
      let "cnt+=1"
    done
    echo "scale=1; $val/$cnt" | bc -l
    ;;
  count)
    cnt=0
    for i in $grep_output; do
      let "cnt+=1"
    done
    echo $cnt
    ;;
esac
